\include "include/global.ly"
\include "include/harmony.ly"
\include "include/outline.ly"
\include "include/tenorB.ly"

\header {
	instrument = \markup \italic \normalsize {
		\override #'(font-name . "Arial")
		"Tbone Bb"
	}
}

\score {
	<<
		% \new ChordNames {
		% 	\set chordChanges = ##t
		% 	\harmony
		% }
		\new Staff <<
			\clef bass
			\NoChords \global \outline \tenorB
		>>
	>>
	\layout {}
}
