\include "articulate.ly"
\include "../include/global.ly"
\include "../include/harmony.ly"
\include "../include/outline.ly"
\include "../include/rhythm.ly"
\include "../include/ebass.ly"

\score {
	\unfoldRepeats \articulate
	\new StaffGroup <<
		% \new ChordNames {
		% 	\AccordiR
		% }
		\new Staff <<
			\set Staff.midiInstrument = "electric bass (finger)"
			\global \outline \ebass
		>>
	>>
	\midi {}
}
