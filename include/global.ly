\version "2.20.0"

\paper {
	#(set-paper-size "a4")          % paper size
	% ragged-last-bottom = ##f      % fill the page till bottom
	% page-count = 1                % force to render just in 1 page
	% between-system-padding = 0    % empty padding between lines
}

NoChords = {
	\override Score.MetronomeMark #'padding = #4
}
WithChords = {
	\override Score.MetronomeMark #'padding = #8
}

\header {
	title = \markup \center-align {
		\override #'(font-name . "Purisa")
		\fontsize #4 \bold
		"Cheek To Cheek"
	}
	subsubtitle = \markup \center-align {
		\override #'(font-name . "Arial")
		"Sinatra version, May arrangement"
	}
	composer=\markup \center-align {
		\override #'(font-name . "Arial")
		"Irving Berlin"
	}
	opus = \markup \tiny {
		\override #'(font-name . "Arial")
		"(1935)"
	}
	tagline = \markup {
		\override #'(font-name . "Arial")
		\tiny \column {
			\fill-line { "Transcription" \italic { "Giuseppe Ricupero" } }
			\fill-line { "Updated" \italic { "07-12-2021 07.48" } }
		}
	}
}

global = {
	\time 4/4
	\key bes \major
	\tempo 4 = 170
	\set Score.skipBars = ##t
	\set Score.countPercentRepeats = ##t
}
