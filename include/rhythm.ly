rhythmPO = { % {{{
	\set Staff.instrumentName = "Chords"
	% INTRO {{{
	\partial 4. r8 r4
	\repeat unfold 8 { s4 s4 s4 s4 } % }}}
	% A {{{
	\repeat unfold 8 { s4 s4 s4 s4 } % }}}
	% B {{{
	\repeat unfold 10 { s4 s4 s4 s4 } % }}}
	% C {{{
	\repeat unfold 9 { s4 s4 s4 s4 } % }}}
	% D {{{
	\repeat unfold 8 { s4 s4 s4 s4 } % }}}
	% E {{{
	\repeat unfold 8 { s4 s4 s4 s4 } % }}}
	% F {{{
	\repeat unfold 8 { s4 s4 s4 s4 } % }}}
	% G {{{
	\repeat unfold 9 { s4 s4 s4 s4 } % }}}
	% H {{{
	\repeat unfold 8 { s4 s4 s4 s4 } % }}}
	% I {{{
	\repeat unfold 8 { s4 s4 s4 s4 } % }}}
	% J {{{
	\repeat unfold 9 { s4 s4 s4 s4 }
	\clef bass
	r8^\markup {\tiny \bold "piano only" } <f,,, f,>4.~ <f f,>2
	% }}}
	% K {{{
	\clef violin
	\repeat unfold 4 { s4 s4 s4 s4 } % }}}
} % }}}

rhythm = { % {{{
	\improvisationOn
	% INTRO {{{
	\partial 4. s4.
	\repeat unfold 8 { c4 c4 c4 c4 } % }}}
	% A {{{
	\repeat unfold 8 { c4 c4 c4 c4 } % }}}
	% B {{{
	\repeat unfold 10 { c4 c4 c4 c4 } % }}}
	% C {{{
	\repeat unfold 9 { c4 c4 c4 c4 } % }}}
	% D {{{
	\repeat unfold 8 { c4 c4 c4 c4 } % }}}
	% E {{{
	\repeat unfold 8 { c4 c4 c4 c4 } % }}}
	% F {{{
	\repeat unfold 8 { c4 c4 c4 c4 } % }}}
	% G {{{
	\repeat unfold 9 { c4 c4 c4 c4 } % }}}
	% H {{{
	\repeat unfold 8 { c4 c4 c4 c4 } % }}}
	% I {{{
	\repeat unfold 8 { c4 c4 c4 c4 } % }}}
	% J {{{
	\repeat unfold 8 { c4 c4 c4 c4 }
	c4-> r4 r2
	s1
	% }}}
	% K {{{
	\repeat unfold 3 { c4 c4 c4 c4 }
	c8-^ c8-^ r8 c8-^ r2
	% }}}
} % }}}

harmonyR = \chordmode { % {{{
	% INTRO {{{
	\partial 4. s4.
	b4:m7 c2.:m7
	f2:9 f4:7 f4:9
	cis4:m7 d2.:m7
	g2:9 g4:7 d4:7.9-

	b4:m7 c2.:m7
	c2:m7 ges4:9 f4:7.9-
	f4:7.9- bes4:dim g2:m7
	c2:m7 f2:13
	% }}}
	% A {{{
	bes2:6 g2:m7
	c2:m7 f2:13
	bes2:6 g2:m7
	c2:m7 f2:13
	
	bes2:6 c2:m6
	des2:dim bes2:6/d
	d1:m7.5-
	g2:7 c2:7
	% }}}
	% B {{{
	f2.:7 des4:dim
	f2:7 ees2:9
	d1:m7.5-
	g1:7

	c1:m7
	c2:m7 c4:m7/f f4:7
	bes2:6 g2:m7
	c2:m7 ges4:9 f4:9

	bes4:6 c4:m7 ges4:9 f4:7.9-
	f4:7.9- bes4:dim bes2:6 
	% }}}
	% C {{{
	c2:m7.5- f2:9
	bes2:6 g2:m7

	c2:m7.5- f2:9
	bes1:6
	c2:m7.5- f2:9
	bes2:6 g2:m7

	c2:m7 f2:13
	bes1:6 
	bes2:6 ees2:m6
	% }}}
	% D {{{
	bes1:m

	bes1:m/aes
	ges1:13
	ges1:13
	f1:7.9-

	f1:7.9-
	bes2:6 g2:m7
	c2:m9 f4:13 f4:9
	% }}}
	% E {{{
	bes2:6 g2:7.5+.9-

	c2:m9 f2:13
	bes2:6 g2:7.5+.9-
	c2:m9 f2:13
	bes2:6 c2:m6

	des2:dim bes2:6/d
	d1:m7.5-
	g2:7 c2:7
	% }}}
	% F {{{
	f2.:7 des4:dim

	f2:7 ees2:9
	d1:m7.5-
	g1:7
	c1:m7

	c2:m7 c4:m7/f f4:7
	bes2:6 c2:m7
	cis2:dim bes4:6/d b4:m7
	% }}}
	% G {{{
	c4:m7 g4:m7 f2:7

	bes2:6 g2:m7
	c4:m7 g4:m7 f2:7
	bes2:6 g2:7.9-
	c2:m7 g4:m7 f4:9

	bes2:6 g4:7.9- b4:m7
	c2:m7 c4:m9 bes:dim
	bes2:6 g4:m7 des4:9
	bes2:6 ees2:m6

	% }}}
	% H {{{
	bes1:m
	bes1:m/aes
	ges1:13
	ges1:13

	f1:7.9-
	f1:7.9-
	bes2:6 g2:m7
	c2:m9 f4:13 f4:7
	% }}}
	% I {{{
	bes2:6 g2:7.5+.9-
	c2:m9 f2:13
	bes2:6 g2:7.5+.9-
	c2:m9 f2:13

	bes2:6 c2:m6
	des2:dim bes2:6/d
	d1:m7.5-
	g1:13
	% }}}
	% J {{{
	f2.:7 des4:dim
	f2:7/c ees2:9
	d1:m7.5-
	g1:9
	
	c1:m7
	f4:9 e4:9 f2:9
	bes1:maj9
	g4:7 fis4:7 g2:7

	c4:m9 s4 s2
	s1
	% }}}
	% K {{{
	bes2:6 ees2:9
	d2:m7 des2:9
	c2:m7 ges4:9 f4:7.9-
	\once \set chordChanges = ##f
	f8:7.9- bes8:dim s8 bes8:6 s2
	% }}}
} % }}}

% vim: ft=lilypond:fdm=marker
