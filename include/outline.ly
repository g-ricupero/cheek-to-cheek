outline = {
	\override DynamicLineSpanner #'staff-padding = #2
	\set Score.markFormatter = #format-mark-box-alphabet
	% ^\markup {\tiny \bold "Intro" }

	% INTRO {{{
	\partial 4. r8 r4 
	s1*4 \break
	s1*4 \break % }}}
	% A,B {{{
	\repeat volta 2 {
		% A {{{
		\mark \default
		s1*4 \break
		s1*4 \break
		\bar "||" % }}}
		% B {{{
		\mark \default
		s1*4 \break
		s1*2 % }}}
	} \alternative {
		{ s1*2 \break }
		{ s1*2 }
	} % }}}
	% C {{{
	\mark \default
	\repeat volta 2 {	
		s1*2 \break
		s1*4 \break
		s1*1
	} \alternative {
		{ s1 }
		{ s1 }
	}
	\bar "||" % }}}
	% D {{{
	\mark \default
	s1   \break
	s1*4 \break
	s1*3
	\bar "||" % }}}
	% E {{{
	\mark \default
	s1   \break
	s1*4 \break
	s1*3
	\bar "||" % }}}
	% F {{{
	\mark \default
	s1   \break
	s1*4 \break
	s1*3 % }}}
	% G {{{
	\mark \default
	\repeat volta 2 {
		s1   \break
		s1*4 \break
		s1*2
	} \alternative {
		{ s1 }
		{ s1 \break }
	}
	\bar "||" % }}}
	% H {{{
	\mark \default
	s1*4 \break
	s1*4 \break
	\bar "||" % }}}
	% I {{{
	\mark \default
	s1*4 \break
	s1*4 \break
	\bar "||" % }}}
	% J {{{
	\mark \default
	s1*4 \break
	s1*4 \break
	s1*2
	\bar "||" % }}}
	% K {{{
	\mark \default
	s1*4
	\bar "|." % }}}
}

% vim: ft=lilypond:fdm=marker
