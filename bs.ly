\include "include/global.ly"
\include "include/harmony.ly"
\include "include/outline.ly"
\include "include/bass.ly"

\header {
	instrument = \markup \italic \normalsize {
		\override #'(font-name . "Arial")
		"Bariton Sax Eb"
	}
}

\score {
	\transpose c a' <<
		% \new ChordNames {
		% 	\set chordChanges = ##t
		% 	\harmony
		% }
		\new Staff <<
			\clef treble
			\NoChords \global \outline \bass
		>>
	>>
	\layout {}
}
